/* Main and helper functions for usync.

   Copyright (C) 2013 Sergio Durigan Junior <sergiodj@riseup.net>

   This file is part of usync.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* 'defs.h' should always be the first file included.  */

#include "defs.h"
#include <argp.h>
#include <string.h>

/* Argument parsing.  */

/* Program version and bug report address.  They are both defined at
   'config.h'.  */

const char *argp_program_version = PACKAGE_VERSION;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;
static const char usync_doc[] = "Synchronizes mail accounts using IMAP/POP/etc";
static const char usync_args_doc[] =
  "[-a|--account ACCOUNT1[,ACCOUNT2...,ACCOUNT<N>]] "

  "[-c|--config-file CONFIG_FILE] "
  "[-e|--verify-config-file] ";

/* Command classification, used to define groups inside the help message.  */

enum
  {
    /* Commands that deal with account(s).  */
    CMD_CLASS_ACC,

    /* Commands that deal with the configuration file.  */
    CMD_CLASS_CONF,
  };

/* The list of options accepted by usync.  It is organized logically and then
   alphabetically.  */

static const struct argp_option usync_options_desc[] = {

    /* Initial number of accounts.  We can grow this number later with
       realloc.  */
#define INIT_MAX_ACCT 10
  { "account", 'a', "ACCOUNT1[,ACCOUNT2,...,ACCOUNT<N>]", 0,
    "Define which accounts will be updated.  Account list must be "
    "comma-separated.",
    CMD_CLASS_ACC },

  { "config-file", 'c', "CONFIG_FILE", 0,
    "Specify a configuration file to be used.  If not specified, "
    "$HOME/.usync/config will be used.",
    CMD_CLASS_CONF },

  { "verify-config-file", 'e', NULL, 0,
    "Verify the configuration file (provided either by `--config-file' or by "
    "$HOME/.usync/config), printing error messages on stderr if found, or "
    "just returning zero if everything is OK.",
    CMD_CLASS_CONF },

  /* Last entry should always be NULL.  */
  { NULL }
};

/* Argument accepted by usync.  */

struct usync_args
  {
    /* List of accounts specified in the command line to be updated.  */
    char **accounts;

    /* Number of accounts specified.  */
    size_t n_accounts;

    /* Name of the config file to be used.  */
    char *conf_file;

    /* Will we just verify the config file?  */
    int verify_conf_file;
  };

/* Function used to parse arguments to command-line parameters.  It returns
   zero on success, or ARGP_ERR_UNKNOWN if it finds an unknown parameter.
   Notice that 'argp_error' terminates the program itself, thus we don't have
   to care with the return value of this function.  */

static error_t
usync_parse_opt (int key, char *arg, struct argp_state *state)
{
  struct usync_args *a = state->input;

  switch (key)
    {
    /* Sets the account(s) to be updated.  */
    case 'a':
      {
	size_t n_entries = 0;
	size_t accounts_size = INIT_MAX_ACCT;
	char *s = arg, *init = arg;

	if (a->accounts != NULL)
	  argp_error (state, "Use the parameter `--account' only once.");
	else if (*s == ',')
	  argp_error (state,
		      "Argument to `--account' cannot start with comma.");

	a->accounts = malloc (accounts_size * sizeof (char *));

	while (s != NULL)
	  {
	    if (*s == ',' || *s == '\0')
	      {
		char c;

		/* Junk at the end of the string, or comma as first char.  */
		if (*s == ',' && s[1] == '\0')
		  argp_error (state,
			      "Extra comma at the end of the argument for "
			      "`--account'.");

		if (n_entries >= accounts_size)
		  {
		    accounts_size *= 2;
		    a->accounts = realloc (a->accounts,
					   accounts_size * sizeof (char *));
		  }

		/* Copy the account name.  Check to see if the user hasn't
		   provided an invalid account name as 'acc1,,acc2'.  */
		if (*init == ',' || *init == '\0')
		  argp_error (state,
			      "Invalid unnamed account.");

		c = *s;
		*s = '\0';
		a->accounts[n_entries] = strdup (init);
		*s = c;
		init = s + 1;

		++n_entries;
	      }

	    if (*s == '\0')
	      break;

	    ++s;
	  }

	/* Reallocating to avoid unused space.  */
	a->accounts = realloc (a->accounts,
			       n_entries * sizeof (char *));
	a->n_accounts = n_entries;
	break;
      }

    /* Sets the configuration file.  */
    case 'c':
      a->conf_file = arg;
      break;

    /* Tells the program to just verify the configuration file.  */
    case 'e':
      a->verify_conf_file = 1;
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }

  return 0;
}

/* The actual declaration of the necessary argp structure.  */

static struct argp usync_argp = {
    usync_options_desc, usync_parse_opt,
    usync_args_doc, usync_doc,
    NULL, NULL, NULL };

/* Main function for usync.  */

int
main (int argc, char *argv[])
{
  struct usync_args usync_args;

  memset (&usync_args, '\0', sizeof (usync_args));

  argp_parse (&usync_argp, argc, argv, 0, NULL, &usync_args);

  exit (EXIT_SUCCESS);
}
