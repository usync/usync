/* Standard definitions for every source file of usync.

   Copyright (C) 2013 Sergio Durigan Junior <sergiodj@riseup.net>

   This file is part of usync.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef HAVE_DEFS_H
#define HAVE_DEFS_H

/* 'config.h' should always be the first file included.  */

#include "config.h"

/* Standard headers.  */

#include <stdio.h>
#include <stdlib.h>

#endif /* !HAVE_DEFS_H */
