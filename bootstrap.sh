#!/bin/sh

# These are the necessary steps to generate the configure scripts.

set -x

AUX_DIR=build-aux

test -d $AUX_DIR || mkdir -p $AUX_DIR

aclocal
autoheader
autoconf
automake --add-missing --copy --force --foreign
